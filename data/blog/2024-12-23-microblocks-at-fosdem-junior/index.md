## FOSDEM Junior 2025

For the second year, MicroBlocks will be part of FOSDEM Junior, bringing the magic of coding and creativity to young minds!

__When:__ February 1-2, 2025

__Where:__ Brussels, Belgium

At FOSDEM Junior, our mission is to inspire the next generation of innovators by introducing them to the world of open-source technology and physical computing in a fun and interactive way. This year, MicroBlocks is stepping up to deliver several exciting workshops designed specifically for children, helping them explore programming through hands-on activities.

No prior experience is needed—just curiosity and a willingness to have fun!

Below are some images from the workshops during this year's FOSDEM Junior.

![image](blog2025-1.png)

![image](blog2025-2.png)

![image](blog2025-3.png)

![image](blog2025-4.png)

### Join Us!
Whether you’re a parent, educator, or open-source enthusiast, we invite you to bring your kids to experience the joy of learning with MicroBlocks. Together, let’s inspire the next wave of developers, engineers, and creators.

Click on the [link](https://fosdem.org/2025/schedule/track/junior/) for the complete FOSDEM Junior schedule.

See you in Brussels!

The MicroBlocks Team