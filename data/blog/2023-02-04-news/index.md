## MicroBlocks at FOSDEM

MicroBlocks will be at FOSDEM 2023 in Brussels this weekend. It will be presented on February 5 in the *FOSS Educational Programming Languages* [track](https://fosdem.org/2023/schedule/track/foss_educational_programming_languages/) organized by Peter Mathijssen, along with Snap!, TurtleStitch, and other free, open-source educational programming languages.

## Recent Articles

MicroBlocks was recently featured [this](https://hackaday.com/2023/02/03/scratch-your-itch-to-program-a-microcontroller/) *Hackaday* article and [this](https://www.cnx-software.com/2023/01/30/microblocks-visual-programming-interface-for-32-bit-microcontrollers/) *CNX SOFTWARE* article.

## Middle Schoolers Learning Robotics to Help People

![](linefollower.gif)

MicroBlocks is being used in an after-school robotics class for middle schoolers that began in January at the Cambridge, Massachusetts public library.

The class, *Robots that Make a Difference*, focuses on using robotic technology to help people. Students start by learning basic robotics principles, then use what they'ved learned to prototype assistive devices. In past years, students created a "bat hat" using distance sensors to warn blind users of walls and obstacles, a mechanical leg for people with leg or spinal injuries, and a head-controlled wheelchair for quadriplegics.

This year, the class will use new technology: MicroBlocks running on a micro:bit combined with the Elecfreaks Wukong board. The rechargeable LiPo battery in the Wukong board will eliminate past issues with batteries while the micro:bit radio system will make it easy for students to program their own wireless control systems. Finally, the liveness of MicroBlocks will make it easier for students to iterate on their designs.

The class runs 13 weeks, ending in early May.
