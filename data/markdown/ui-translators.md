We'd welcome your help in localizing the MicroBlocks editor. Our locales can be translated online by anyone in Weblate.
