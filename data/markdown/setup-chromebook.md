#### Chromebook Setup ####

Google is phasing out Chrome apps. Thus, for most Chromebook users, it is
better to run MicroBlocks in the browser instead of installing the app.

However, some older Chromebooks have reached their
[end of life](https://support.google.com/chromebook/answer/9367166?hl=en)
and cannot be updated to a version of Chrome that supports webserial (v89 or later).
Fortunately, such Chromebooks can still run the MicroBlocks Chrome app.

Go to the [Download](download) page and click the **Download** button.
If are on a Chromebook, that will bring you to the
[MicroBlocks page](https://chrome.google.com/webstore/detail/microblocks/cbmcbhgijipgdmlnieolilhghfmnngbb?authuser=0)
on the Chrome Web Store.

<img src="assets/img/md/get-started/chrome-web-store.png" width="400">

Click the **Add to Chrome** button to install the app.

To run, open the Chromebook app launcher and click the MicroBlocks bunny icon.

You can pin the MicroBlocks app to make it easy to open in the future.
