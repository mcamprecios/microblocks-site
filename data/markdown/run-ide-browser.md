To run MicroBlocks in the Chrome or Edge browser,
click **Run** in the navigation bar at the top of the screen.

<img src="assets/img/md/get-started/browser-run-button.png" width="70">

Plug in your board, then click the USB icon.

<img src="assets/img/md/get-started/usb-icon.png" width="50">

Select your board from the browser dialog, then click **Connect**.

<img src="assets/img/md/get-started/browser-connect-dialog.png" width="400">

If you have installed the MicroBlocks firmware (see [Board Setup](#board))
a green circle should appear behind the USB icon.

<img src="assets/img/md/get-started/connected.png" width="150">

For a quick introduction to using MicroBlocks, check out
[this video](https://www.youtube.com/watch?v=cf2xsYSTqgY) (micro:bit) or
[this one](https://www.youtube.com/watch?v=ayLqWwqItxg) (Circuit Playground Express or Bluefruit).
You can also explore the <a href="https://wiki.microblocks.fun/ide" target="_blank">user guide</a>
and <a href="https://wiki.microblocks.fun/reference_manual" target="_blank">blocks reference manual</a>.
