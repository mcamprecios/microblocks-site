Want to translate or remix an activity card? They are all available
[here](https://drive.google.com/open?id=1BxsJ-nEC1MxT67O2bfrHBS9b8nmuMDad)
in Google Slides/Docs format under the Creative Commons CC BY-SA 4.0 license.
Please send us a link to your translations so we can share them with others.
