#### micro:bit (v1 or v2) ####

Connect the board to your computer.

From the MicroBlocks menu (gear icon), select **update firmware on board**.

<img src="assets/img/md/get-started/update-firmware-menu.png" width="180">

The yellow LED will flicker as the firmware is installed. It takes a few seconds.

If you are running the MicroBlocks app, MicroBlocks will connect to the board automatically
when it is done.

[This video](https://www.youtube.com/watch?v=V4u2_GN8JnU) shows these steps.

**Extra Steps in Browser**

If you are running MicroBlocks in the browser or as a web app, you need to help the browser.
(For security reasons, the browser cannot access the board's USB drive without asking the user.)

First, select your board type from the menu.

<img src="assets/img/md/get-started/select-microbit.png" width="180">

A dialog box will appear. Read the instructions, then click **OK**.

<img src="assets/img/md/get-started/firmware-install-instructions-microbit.png" width="320">

In the browser's save dialog, select your **Downloads** folder, then click the **Save** button.

<img src="assets/img/md/get-started/firmware-save-dialog-microbit.png" width="500">

Finally, drag the firmware file to the **MICROBIT** drive.

<img src="assets/img/md/get-started/drag-to-microbit-drive.png" width="320">

You will see the orange light on the board flicker as the firmware is installed.
You will also see a progress indicator on the screen.

<img src="assets/img/md/get-started/firmware-progress.png" width="320">

When this installation is done, click the USB icon to connect to the board.

<img src="assets/img/md/get-started/connect-to-board.png" width="320">
