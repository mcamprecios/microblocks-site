#### No Setup Needed! ####

You don't need to install anything to run MicroBlocks in a Chrome or Edge browser;
just click **Run** in the navigation bar at the top of the screen.

<a href="/run"><img src="assets/img/md/get-started/browser-run-button.png" width="70"></a>

Running MicroBlocks in the browser avoids the need to install a conventional application,
a process that often requires IT assistance.

MicroBlocks will run in other browsers, but it can only connect to a board when
run in Chrome or Edge on a desktop, laptop, or Chromebook computer (not a mobile device).

#### Optional: Save the MicroBlocks Web App ####

For convenience, you can save a copy of MicroBlocks as a "progressive web app"
that you can launch via a shortcut icon like a conventional application.
Once saved, the MicroBlocks web app even runs offline!

To save the MicroBlocks web app, run MicroBlocks in your browser, then click the
**install** button on the top right of the browser's URL bar:

Chrome:<br>
<img src="assets/img/md/get-started/pwainstall-chrome.png" width="220">

Edge:<br>
<img src="assets/img/md/get-started/pwainstall-edge.png" width="200">

That will install the web app and open MicroBlocks in a new window.
It will also add a shortcut to your desktop so you can launch the web app later.
