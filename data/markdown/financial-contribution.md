MicroBlocks is a not-for-profit project under the fiscal sponsorship of the Software Freedom Conservancy, a 501(c)(3) non-profit.

MicroBlocks can accept tax-deductible donations, grants, and sponsorships from individuals and organizations. If you find MicroBlocks useful, we invite you or your organization to contribute.

Click the button below to make a personal donation to the project via PayPal or credit card.

If you would like to make a large donation (over $500) via wire transfer (which saves us the PayPal service fee) or sponsor this project in some other way, please contact us.

Thank you. Every little bit helps!
