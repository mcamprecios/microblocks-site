# MicroBlocks Site Front-end

MicroBlocks Site Front-end - 2021-05-18

## Pages meta data

### Global pages metadata

Pages accept metadata. It all is optional but the title, and when it's not passed to the template, the standard information is used.

To pass it to the template:

```
{{#> layouts._html-shell
    pageClass="page-article"
    pageUrl="https://wwww.microblocks.fun/article"
    opengraphPicture="thumbnail.jpg"
}}
```

The following fields are available:

- ```title``` (Required)
- ```pageClass```: CSS class
- ```pageUrl```
- ```pageDescription```
- ```opengraphPicture```:For social media sharing

### Blog metadata

On top of the global page's metadata, Blog posts are built with some extra fields. Each article must have a ```meta.json``` file in its directory.

The following fields are available:

- ```title``` (Required)
- ```author```
- ```author-link```
- ```organization```
- ```organization-link```
- ```thumbnail-copyright```
- ```last-update```
